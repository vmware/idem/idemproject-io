# !ARCHIVED!

This project has been archived, along with all other POP and Idem-based projects.
- For more details: [Salt Project Blog - POP and Idem Projects Will Soon be Archived](https://saltproject.io/blog/2025-01-24-idem-pop-projects-archived/)

# idemproject.io Landing

> NOTE: Currently, https://www.idemproject.io serves as the primary website landing for Idem Project, and this repository should not be used unless `https://idemproject.io` is meant to become anything more than a redirect.
